/* Copyright (C) 2012,2013 HiPro IT Solutions Private Limited,
 * Chennai. All rights reserved.
 *
 * This program and the accompanying materials are made available
 * under the terms described in the LICENSE file which accompanies
 * this distribution. If the LICENSE file was not attached to this
 * distribution or for further clarifications, please contact
 * legal@hipro.co.in. */

#ifndef HIPRO_THITTAM__bbd3b644_fd93_11e2_a709_001f3c9e2082
#define HIPRO_THITTAM__bbd3b644_fd93_11e2_a709_001f3c9e2082

#include <hdb/log.h>

#define Log_I HDB_LOG_INFO (m_logger)
#define Log_W HDB_LOG_WARNING (m_logger)
#define Log_E HDB_LOG_ERROR (m_logger)
#define Log_F HDB_LOG_CRITICAL (m_logger)
#define Log_D1 HDB_LOG_DEBUG1 (m_logger)
#define Log_D2 HDB_LOG_DEBUG2 (m_logger)
#define Log_D3 HDB_LOG_DEBUG3 (m_logger)
#define Log_D4 HDB_LOG_DEBUG4 (m_logger)

#define THROW  HDB_LOG_THROW
#define ASSERT HDB_LOG_ASSERT

#endif // HIPRO_THITTAM__bbd3b644_fd93_11e2_a709_001f3c9e2082

/*
  Local Variables:
  mode: c++
  indent-tabs-mode: nil
  tab-width: 4
  c-file-style: "gnu"
  End:
*/
